# dotfiles

Configuration and dotfiles are version-controlled in a bare repository.

## Import repository configuration files locally

1. Add a dotfiles alias, a git command always operating from bare dotfiles repository to home working tree.

```bash
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
```

2. Create a gitignore file in home directory, with a .dotfiles entry.

3. Clone the dotfiles repository.

```bash
git clone --bare https://gitlab.com/antoine.ouellet/dotfiles.git > $HOME/.dotfiles
```