"""Qtile configuration file.

This configuration is opiniated for 14" laptop use on X11.

External dependencies (as named in Arch/AUR):
    - brightnessctl
    - pamixer
    - ttf-ibm-plex
    - python-iwlib
    - python-pybluez
    - python-dbus-next
"""
import os
import re

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal


mod = "mod4"
terminal = guess_terminal()
os.environ["QT_QPA_PLATFORMTHEME"] = "kde"
os.environ["QT_STYLE_OVERRIDE"] = "Breeze-dark"
os.environ["GTK_THEME"] = "Adwaita:dark"

rofi_call = 'rofi -show drun icon-theme "Papirus" -show-icons -theme Arc-Dark'
icons_path = '~/.local/share/icons/ePapirus'

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn("alacritty -o font.size=7.8"),
        desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "f", lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window"),
    Key([mod], "t", lazy.window.toggle_floating(),
        desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([mod], "d", lazy.spawn(rofi_call)),

    # Fn keys.
    Key([], "XF86AudioMute", lazy.spawn("pamixer --toggle-mute")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 1")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 1")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +5%")),
]


groups = [
    Group("1:home"),
    Group("2:web", matches=[Match(wm_class='firefox')]),
    Group("3:dev"),
    Group("4:sci", matches=[Match(title=re.compile('QGIS'))], layout="max"),
    Group("5:doc"),
    Group("6:img"),
    Group("7:desk"),
    Group("8:msn"),
    Group("9:media"),
    Group("10:fun"),
]

group_keys = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

for group, group_key in zip(groups, group_keys):
    keys.extend([
        # mod1 + group_key = switch to group
        Key(
            [mod],
            group_key,
            lazy.group[group.name].toscreen(),
            desc="Switch to group {}".format(group.name),
        ),
        # mod1 + shift + group_key = switch to & move window to group
        Key(
            [mod, "shift"],
            group_key,
            lazy.window.togroup(group.name, switch_group=True),
            desc=f"Switch to & move focused window to group {group.name}",
        ),
    ])

layouts = [
    layout.Columns(
        border_normal="011f4b",
        border_focus="#005b96",
        border_focus_stack=["#eeeeee", "#000000"],
        border_width=2
    ),
    layout.Max(),
    layout.Floating(),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    foreground="#222222",
    background="#cccccc",
    font="IBM Plex Sans",
    fontsize=16,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper="~/.local/share/wallpapers/HD KDE Plasma Scenery 174.png",
        wallpaper_mode="stretch",
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(background="333333"),
                widget.GroupBox(
                    active="#222222",
                    inactive="#ffffff",
                    block_highlight_text_color="#ffffff",
                    highlight_method="line",
                    highlight_color="#005b96",
                    #font="IBM Plex Sans MedM",
                ),
                widget.Prompt(),
                widget.Spacer(length=bar.STRETCH),
                widget.WindowName(font="IBM Plex Sans SmBld"),
                widget.Spacer(length=bar.STRETCH),
                widget.PulseVolume(
                    fmt=" {} ",
                    volume_app="pamixer",
                    volume_up_command="pamixer -i 5",
                    volume_down_command="pamixer -d 5",
                    get_volume_command="pamixer --get-volume-human",
                    mute_format="\u2298",
                ),
                widget.Systray(),
                widget.TextBox(
                    "\u2314",
                    fmt="<b>{}</b>",
                    fontsize=40,
                    padding=0,
                ),
                widget.Wlan(
                    format="{essid} ({quality}/70) ",
                    disconnected_message=" <b>NW</b> disconnected ",
                ),
                widget.Bluetooth(
                    fmt=' <b>\u16E1\u16D2</b>{}',
                    default_text=" {connected_devices} ",
                    mouse_callbacks={
                        "Button1": lazy.spawn("bluedevil-wizard")
                    },
                ),
                widget.Battery(
                    full_char="\U0001F50C",
                    charge_char="\U0001F50C",
                    discharge_char="\U0001F50B",
                    empty_char="\U0001FAAB",
                    format=(
                        " <b>{char}</b> {percent:2.0%} ({hour:d}h{min:02d}) "
                    ),
                ),
                widget.Clock(
                    format=" \U0001F4C5 %A | %Y-%m-%d | %H:%M ",
                ),
                widget.QuickExit(
                    default_text="\u23FB",
                    fmt=" <b>{}</b> ",
                    countdown_format="<b>{}</b>",
                    fontsize=24,
                ),
            ],
            size=24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = False
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# 3D non-reparenting WM written in java on java's whitelist.
wmname = "LG3D"

